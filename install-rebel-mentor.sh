#!/bin/bash

# colors
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# Get files from repo
echo -e "${GREEN}Begin setting up Rebel Mentor them files${BLUE}"
git clone --depth 1 git@gitlab.com:d3fy/rebel-mentor.git
echo -e "${YELLOW}DONE"

# move top level files over
echo -ne "${BLUE}Moving .gitignore movefile.yml and env-example.txt..."
mv rebel-mentor/.gitignore rebel-mentor/movefile.yml rebel-mentor/env-example.txt .
echo -e "${YELLOW}DONE"

# move theme folder over
echo -ne "${BLUE}Moving theme into themes folder..."
mv rebel-mentor/wp-content/themes/rebel-mentor/ wp-content/themes/rebel-mentor
echo -e "${YELLOW}DONE"

# remove leftovers from git clone
echo -ne "${BLUE}Remove cloned stuff..."
rm -rf rebel-mentor/
echo -e "${YELLOW}DONE"

# remove install.sh
echo -ne "${BLUE}Remove install.sh file..."
rm install-rebel-mentor.sh
echo -e "${YELLOW}DONE\n"

# closing satements
echo -e "${GREEN}ALL DONE!!! Time to start building! ${NC}"
