<?php
  
namespace RebelMentor\Theme;
  
if ( ! defined( 'ABSPATH' ) ) exit;
  
// define shared constants
define( 'REBELMENTOR_PATH', untrailingslashit( get_template_directory() ) );
define( 'REBELMENTOR_URI', untrailingslashit( get_template_directory_uri() ) );
define( 'REBELMENTOR_TEXTDOMAIN', 'rebelmentor' );
define( 'REBELMENTOR_PREFIX', REBELMENTOR_TEXTDOMAIN );
define( 'REBELMENTOR_VERSION', '20180910A' );
define( 'REBELMENTOR_DEVMODE', 1);

// class autoloader
/*
spl_autoload_register( function( $classname ) {
  $namespace = 'RebelMentor\\';
  if ( substr( $classname, 0, strlen( $namespace ) ) !== $namespace ) return;
  // Enter each folder you wish to autoload files from (under includes/classes)
  foreach ( array( 'Site', 'Admin', 'posttypes', 'shortcodes', 'widgets' ) as $sub ) {
    $subnamespace = $namespace . $sub . '\\';
    if ( substr( $classname, 0, strlen( $subnamespace ) ) === $subnamespace ) {
      $filename = strtolower(
        str_replace( '\\', '/', substr( $classname, strlen( $subnamespace ) ) )
      ) . '.php';
      $path = REBELMENTOR_PATH . '/includes/classes/' . strtolower( $sub ) . '/' . $filename;
      if ( is_readable( $path ) ) { require_once( $path ); break; }
    }
  }
}, true, true );
*/

// load main theme class
$Theme = Theme::instance();