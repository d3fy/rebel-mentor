<?php

namespace RebelMentor\Theme;

get_header();

if ( have_posts() ) {
  while ( have_posts() ) {
    the_post();
    the_content( '<p class="serif">Read the rest of this page &raquo;</p>' );
  }
}

get_footer();
