jQuery(document).ready(function($) {
  $('body').addClass('with-js');
  
  // get viewport size (with scrollbar, to match media queries)
  var getViewportSize = function() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    var viewportSize = { 'width': e[a+'Width'], 'height': e[a+'Height'] };
    var wpAdminBarHeight = $("#wpadminbar").height();
    if (wpAdminBarHeight) { viewportSize.height -= wpAdminBarHeight; }
    return viewportSize;
  };
  
  // get numeric value of string
  var getNumVal = function(something) {
    var results = something.toString();
    results = results.replace(/,/g, '');
    results = parseFloat(results);
    return (isNaN(results)) ? 0 : results;
  };
  
  // format number for currency output
  var formatCurrency = function(something) {
    var results = getNumVal(something);
    results = results.toFixed(2);
    return results.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  
  // check for resize, but wait a bit until resize is finished
  $(window).resize(function() {
    if (this.resizeTO) {
      clearTimeout(this.resizeTO);
    }
    this.resizeTO = setTimeout(function() {
      $(this).trigger('resizeEnd');
    }, 300);
  });
  
  // add class to affixed menu when not at top of page
  updateBodyScrollClass = function(){
    if ($(window).scrollTop() > 1){
      if (!$("body").hasClass("scrolled-down")) {
        $("body").addClass("scrolled-down");
      }
    } else {
      if ($("body").hasClass("scrolled-down")) {
        $("body").removeClass("scrolled-down");
      }
    }
  };
  $(window).on("scroll", updateBodyScrollClass);
  updateBodyScrollClass();
  
  // mobile navigation toggle
  $(".topnav-toggle").click(function(e) {
    e.preventDefault();
    var $target = $("body");
    if ($($target).hasClass("topnav-opened")) {
      $($target).removeClass("topnav-opened");
      $(this).addClass("closed");
    } else {
      $($target).addClass("topnav-opened");
      $(this).removeClass("closed");
    }
  });
  
  // offset absolute positioned top bar by WP admin bar height
  var offsetHeaderBar = function() {
    var wpAdminBarHeight = getNumVal($("#wpadminbar").height());
    $("#navbar-top").css({"top": wpAdminBarHeight+"px"});
  }
  offsetHeaderBar();
  $(window).bind('resizeEnd', function() {
    offsetHeaderBar();
  });
  
  // expandable element content toggle
  var showMoreBtn = '<button class="read-more-btn elementor-button elementor-size-sm">Read More <span class="fa fa-chevron-down"><\/span><\/button>';
  var showLessBtn = '<button class="show-less-btn elementor-button elementor-size-sm">Show Less <span class="fa fa-chevron-up"><\/span><\/button>';
  $(".show-more-block .elementor-widget-container").each(function() {
    $(this).addClass("collapsed");
    $(this).wrapInner('<div class="block-content-wrapper"><\/div>');
    $(this).append('<div class="show-more-less-toggle"><div class="content-fade"><\/div><div class="toggle-btn">'+showMoreBtn+'<\/div><\/div>');
  });
  $(document).on("click", ".show-more-block .read-more-btn", function(e) {
    e.preventDefault();
    $wrapper = $(this).closest(".elementor-widget-container");
    $($wrapper).removeClass("collapsed").addClass("expanded");
    $(".toggle-btn", $($wrapper)).html(showLessBtn);
  });
  $(document).on("click", ".show-more-block .show-less-btn", function(e) {
    e.preventDefault();
    $wrapper = $(this).closest(".elementor-widget-container");
    $($wrapper).removeClass("expanded").addClass("collapsed");
    $(".toggle-btn", $($wrapper)).html(showMoreBtn);
  });
  
});
