<?php

namespace RebelMentor\Theme;


/**
 * Theme Class
 *
 * Loads all theme settings.
 *
 * @since 1.0.0
 */
class Theme {
  
  // properties
  private static $instance;
  
  // singleton instance
  public static function instance() {
    if ( ! ( self::$instance instanceof self ) ) self::$instance = new self();
    return self::$instance;
  }
  
  // constructor
  private function __construct() {
    add_action( 'after_setup_theme', array( $this, 'setup' ) );
  }
  
  // load theme
  public function setup() {
    
    // load things on theme setup
    $this->load_supports();
    $this->load_menus();
    $this->load_sidebars();
    $this->load_shortcodes();
    $this->load_posttypes();
    $this->load_widgets();
    $this->load_action_hooks();
    $this->load_filters();
    
    // misc. theme features
    load_plugin_textdomain( REBELMENTOR_TEXTDOMAIN );
    load_theme_textdomain( REBELMENTOR_TEXTDOMAIN, REBELMENTOR_PATH . '/languages' );
  }
 
  // load theme support
  public function load_supports() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array( 
      'comment-list', 'search-form', 'comment-form', 'gallery', 'caption'
    ) );
    add_image_size('custom_image_size', 1170, 400, true); 
  }
  
  // load menus
  public function load_menus() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
  }
  
  // load scripts
  public function load_scripts() {
    $v = ( REBELMENTOR_DEVMODE ) ? time() : REBELMENTOR_VERSION;
    wp_enqueue_script( 'jquery', null, null, null, true );
    wp_enqueue_script( 'hoverIntent', null, null, null, true );
    // custom theme js
    wp_enqueue_script(
      'rebelmentor-js-theme',
      REBELMENTOR_URI . '/assets/scripts/theme.min.js',
      array('jquery', 'hoverIntent'), $v, true
    );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );
  }
  
  // load styles
  public function load_styles() {
    $v = ( REBELMENTOR_DEVMODE ) ? time() : REBELMENTOR_VERSION;
    wp_enqueue_style( 'rebelmentor-style', REBELMENTOR_URI . '/assets/styles/main.min.css', array(), $v );
    add_editor_style( array( REBELMENTOR_URI . '/assets/styles/editor-styles.css' ) );
  }
  
  // load widget areas / sidebars
  public function load_sidebars() {}
  
  // load widgets
  public function load_widgets() {}
  
  // load action hooks
  public function load_action_hooks() {
    add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ), 25 );
    add_action( 'wp_enqueue_scripts', array( $this, 'load_styles' ), 25 );
    add_action( 'widgets_init', array( $this, 'load_sidebars' ) );
    add_action( 'widgets_init', array( $this, 'load_widgets' ) );
  }

  // load filters
  public function load_filters() {
    add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
  }
  
  // load shortcodes
  public function load_shortcodes() {
    include( REBELMENTOR_PATH . '/includes/shortcodes/antispambot.php' );
    include( REBELMENTOR_PATH . '/includes/shortcodes/datetime.php' );
    include( REBELMENTOR_PATH . '/includes/shortcodes/icon.php' );
  }
  
   // load posttypes
  public function load_posttypes() {
    include( REBELMENTOR_PATH . '/includes/posttypes/sample_posttype.php' );
  }
 
} // End Theme Class
