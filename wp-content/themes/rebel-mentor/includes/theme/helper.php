<?php

namespace RebelMentor\Theme;

class Helper
{
  
  //------------------------------------------------------------------------
  // PATHS
  //------------------------------------------------------------------------
    
  // get URL path to child theme directory (css, js, etc.)
  public static function getChildThemeURI() {
    return dirname( get_stylesheet_uri() );
  }
  
  // get system path to child theme directory (includes)
  public static function getChildThemePath() {
    return get_stylesheet_directory();
  }
  
  // get URL path to parent theme directory (css, js, etc.)
  public static function getParentThemeURI() {
    return get_template_directory_uri();
  }
  
  // get system path to parent theme directory (includes)
  public static function getParentThemePath() {
    return get_template_directory();
  }
  
  // get file path, from child theme if available, otherwise from parent
  public static function getThemePath( $file ) {
    $childPath = trailingslashit( self::getChildThemePath() ).$file;
    $parentPath = trailingslashit( self::getParentThemePath() ).$file;
    if ( file_exists( $childPath ) ) return $childPath;
    else if ( file_exists( $parentPath ) ) return $parentPath;
    else return null;
  }
  
  // get file path, from child theme if available, otherwise from parent
  public static function getThemeURI( $file ) {
    $childPath = trailingslashit( self::getChildThemePath() ).$file;
    $childURI = trailingslashit( self::getChildThemeURI() ).$file;
    $parentPath = trailingslashit( self::getParentThemePath() ).$file;
    $parentURI = trailingslashit( self::getParentThemeURI() ).$file;
    if ( file_exists( $childPath ) ) return $childURI;
    else if ( file_exists( $parentPath ) ) return $parentURI;
    else return null;
  }
  
  // get file path, from child theme if available, otherwise from parent
  public static function getTmplPath( $file ) {
    $file = self::cleanFileName( $file );
    return ($file) ? self::getThemePath( 'includes/tmpl/'.$file ) : null;
  }
  
  // get the current page request URI
  public static function getCurrentURI( $opts=array() ) {
    $opts = new Registry( $opts );
    $uri = self::getArrayVal( $_SERVER, 'REQUEST_URI' );
    if ( $opts->get('remove_query') ) $uri = strtok( $uri, '?' );
    return $uri;
  }
  
  //------------------------------------------------------------------------
  // THEME OPTIONS
  //------------------------------------------------------------------------
  
  // get value from bootpalette theme option (or default)
  public static function getOption( $key, $default='' ) {
    static $options = null;
    if ($options === null) {
      $options = get_option( 'REBELMENTOR_options' );
      if ( !is_array( $options ) ) $options = array();
    }
    return ( array_key_exists( $key, $options ) ) ? $options[$key] : $default;
  }
  
  //------------------------------------------------------------------------
  // MISCELLANEOUS
  //------------------------------------------------------------------------
  
  // get value from array, or default if not there
  public static function getArrayVal( &$array, $key, $default='' ) {
    if ( is_array( $array ) && array_key_exists( $key, $array ) ) {
      return $array[$key];
    } else return $default;
  }
  
  // clean up file name using character whitelist
  public static function cleanFileName( $filename ) {
    return preg_replace( '/[^A-Z0-9_\-\.]/i', '', $filename );
  }
  
  // extract phone number link from string
  public static function getPhoneLink( $string ) {
    $phone = preg_replace( '/[^0-9]/i', '', $string );
    if ( strlen( $phone ) == 11 && substr( $phone, 0, 1 ) == '1' )
      $phone = substr( $phone, 1 );
    if ( strlen( $phone ) === 10 ) {
      $url = 'tel:+1-' . substr( $phone, 0, 3 ) . '-' . substr( $phone, 3, 3 ) . '-' . substr( $phone, 6 );
    } else $url = '';
    return $url;
  }

}
