<?php

namespace RebelMentor\Theme;

class Dashboard {
  
  // class constructor method
  public function __construct() {
    add_action('wp_dashboard_setup', array( $this, 'shortcode_info_widget' ) );
  }
  
  // shortcodes info
  public function shortcode_info_widget() {
    wp_add_dashboard_widget(
      'app_dashboard_widget_shortcodes',
      'Custom Theme Shortcodes',
      array( $this, 'shortcode_info_content' )
    );
  }
  
  // shortcode info content
  public function shortcode_info_content() {
    ?>
    <p>
      Anti-Spambot Email Links:<br />
      <code>[antispambot email="" text=""]</code>
    </p>
    <hr style="border-top: 1px dotted #ddd;" />
    <p>
      Current Date/Time:<br />
      <code>[datetime format="Y-m-d"]</code>
    </p>
    <hr style="border-top: 1px dotted #ddd;" />
    <p>
      Icon:<br />
      <code>[icon class="fa fa-chevron-right"]</code>
    </p>
    <hr style="border-top: 1px dotted #ddd;" />
    <?php
  }
  
}
