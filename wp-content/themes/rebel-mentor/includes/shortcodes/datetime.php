<?php

namespace RebelMentor\Theme;

// shortcode class - insert WP menu into content
class Shortcode_Datetime {
  
  // constructor (optionally auto-register shortcode)
  public function __construct( $register=true ) {
    if ( $register ) $this->register();
  }
  
  // register the shortcode with WordPress
  public function register() {
    add_shortcode( 'datetime', array( $this, 'shortcode' ) );
  }
  
  // shortcode handler
  public function shortcode( $incoming, $content=null ) {
    $defaults = array( 'format'=>'Y' );
    $overrides = array();
    $args = array_merge(
      shortcode_atts( $defaults, $incoming ),
      $overrides
    );
    $args['format'] = (string) $args['format'];
    return date( $args['format'], time() );
  }
  
}

$shortcode = new Shortcode_Datetime();
