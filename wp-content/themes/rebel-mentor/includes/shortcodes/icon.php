<?php

namespace RebelMentor\Theme;

// shortcode class - insert WP menu into content
class Shortcode_Icon {
  
  // constructor (optionally auto-register shortcode)
  public function __construct( $register=true ) {
    if ( $register ) $this->register();
  }
  
  // register the shortcode with WordPress
  public function register() {
    add_shortcode( 'icon', array( $this, 'shortcode' ) );
  }
  
  // shortcode handler
  public function shortcode( $incoming, $content=null ) {
    $defaults = array( 'class'=>'' );
    $overrides = array();
    $args = array_merge(
      shortcode_atts( $defaults, $incoming ),
      $overrides
    );
    return '<span class="' . esc_attr( $args['class'] ) . '"></span>';
  }
  
}

$shortcode = new Shortcode_Icon();
