<?php

namespace RebelMentor\Theme;

// shortcode class - insert WP menu into content
class Shortcode_Antispambot {
  
  // constructor (optionally auto-register shortcode)
  public function __construct( $register=true ) {
    if ( $register ) $this->register();
  }
  
  // register the shortcode with WordPress
  public function register() {
    add_shortcode( 'antispambot', array( $this, 'shortcode' ) );
  }
  
  // shortcode handler
  public function shortcode( $incoming, $content=null ) {
    $defaults = array( 'email'=>'' );
    $overrides = array();
    $args = array_merge(
      shortcode_atts( $defaults, $incoming ),
      $overrides
    );
    $output = '';
    if ( is_email( $args['email'] ) ) {
      if ( !$args['text'] ) $args['text'] = $args['email'];
      $output = '<a href="mailto:' . antispambot( $args['email'] ) . '">' . antispambot( $args['text'] ) . '</a>';
    }
    return $output;
  }
  
}

$shortcode = new Shortcode_Antispambot();
