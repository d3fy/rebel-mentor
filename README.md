# Rebel Mentor: WordPress Starter Theme For Elementor

**Plus additional workflow tools like .gitignore for wordpress and WordMove configs**

A simple theme with only the basics + useful libraries. Specifically made to work with the Elementor Page builder.

**Use Elementor for:** setting font colors, setting font family, and all page/post/archive layouts.

**Use Theme files for:** setting default font sizes, default form element sizes, and custom post type setup.

## Features

- "Include Media" for easy media queries: https://include-media.com/
- "PostTypes" for easy post type creation: https://github.com/jjgrainger/PostTypes
- "WordMove" for easy push/pull to staging or production: https://github.com/welaika/wordmove
- "GitIngore for wordpress" for efficient repo usage: https://github.com/bigwilliam/gitignore-for-wordpress

## Dependencies

- git
- composer
- gulp, webpack or codekit (TODO: set one up)

## Getting Started

It's not simple to just clone this repo to get started, so use this method instead:

From the root of your WordPress directory in the terminal run:

`curl -o install-rebel-mentor.sh https://gitlab.com/d3fy/rebel-mentor/raw/master/install-rebel-mentor.sh && chmod +x install-rebel-mentor.sh && ./install-rebel-mentor.sh`

The script will clone the repo and move all the files into the right places, then auto-delete when finished.

Next, copy env-example.txt to `.env` and update variables to match your environments. Note: Wordmove mirrors the sites, so make sure you update movefile.yml accordingly (see link to WordMove under "Features").

## TO DO

- set up webpack, codekit, or gulp
- WP Admin & Dashboard customizations

## Contributing

- This project has only been tested on Mac OSX, and we welcome anyone who would like to test on Linux or Windows.
- submit issues or merge requests here in Gitlab
- email us at developers@d3fy.com to get in touch
